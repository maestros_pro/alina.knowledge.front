
module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH;
	return ()=> gulp.src([`${PATH.FONTS.SOURCE}**/*`])
		.pipe(gulp.dest(PATH.FONTS.PUBLIC));
};
