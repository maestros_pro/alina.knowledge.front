
module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH;
	return ()=> gulp.src(`${PATH.PAGES.SOURCE}*.pug`)
		.pipe($.pug({
			pretty: '\t'
		}).on('error', function(error){
			$.util.log(error.message);
			this.emit('end');
		}))
		.pipe(gulp.dest(PATH.PAGES.PUBLIC))
		.on('error', function(error){
			$.util.log(error.message);
			this.emit('end');
		});
};
