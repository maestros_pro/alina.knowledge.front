module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH, argv = args.argv,
		format = [
			// 'png',
			// 'jpg',
			'svg'
		];

	return ()=> {

		for ( let i = 0 ; i < format.length; i++){
			let pathFormat = `${PATH.IMG.SOURCE}sprites/${format[i]}`;

			args.fs.readdir(pathFormat, (err, items) => {
				if (items){
					for ( let j = 0 ; j < items.length; j++){
						// console.log(format[i] + '/' + items[j]);
						if ( !items[j].indexOf('.') >= 0 ) {
							// console.log(format[i]);
							if ( format[i] === 'svg'){
								gulp.src([`${PATH.IMG.SOURCE}sprites/svg/**/*.svg`])
									.pipe($.svgSprite({
										svg: {
											precision: 2
										},
										shape: {
											id:{
												separator: '-'
											}
										},
										mode: {
											view:  false,
											css: {
												dest: `../../`,
												bust: false,
												// prefix: '',
												sprite: `${PATH.IMG.SOURCE}${items[j]}`,
												render: {
													scss: {
														dest: `${PATH.CSS.SOURCE}sprites/_${items[j]}.scss`,
														template: `gulp-tasks/sprite.template.scss`
													}
												},
												variables: {
													spritesName: items[j],
													spritesURL: `../img/${items[j]}.svg`
												}
											}
										}
									}))
									.on('error', function(error){
										$.util.log(error.message);
										this.emit('end');
									})
									.pipe(gulp.dest(PATH.IMG.PUBLIC))
								;
							} else {
								/*let pathSprite = SOURCES_DIR + '/img/sprites/' + format[i] + '/' + items[j];
								let spriteData =
									gulp.src(pathSprite + '/!*.*')
										.pipe(spritesmith({
											imgName: items[j] + '.' + format[i],
											cssName: items[j] + '.scss',
											imgPath: '/img/' + items[j] + '.' + format[i],
											cssVarMap: function (sprite) {
												sprite.name = items[j] + '-' + sprite.name;
											}
										}));
								spriteData.img
									.pipe(buffer())
									.pipe(imagemin())
									.pipe(gulp.dest(PUBLIC_DIR + '/img/'));
								spriteData.css.pipe(gulp.dest(SOURCES_DIR + '/css/sass/sprites'));*/
							}
						}
					}
				}
			});
		}
	}
};
