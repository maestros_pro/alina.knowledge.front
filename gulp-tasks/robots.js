
module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH;
	return ()=> gulp.src([`${PATH.DIR.SOURCE}robots.txt`])
		.pipe(gulp.dest(PATH.DIR.PUBLIC));
};
