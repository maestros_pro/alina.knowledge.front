$(function () {

	$('body')
		.on('submit', '.registration-popups .form', function (e) {

			var $form = $(this),
				wrong = !!$form.attr('data-form-validate-error')
			;

			if ( wrong ){
				e.preventDefault();
			} else if( $form.hasClass('form_reg') && !$form.attr('data-registered') ) {
				e.preventDefault();
				$.ajax({
					url: $form.attr('data-check-url'),
					data: $form.serialize()
				}).done(function(data){
					if (data.status === true){
						$form
							.attr('data-registered', 'true')
							.closest('.popup__auth')
							.addClass('is-registered')
							.find('.form_pass')
							.find('input[type = email]')
							.val($form.find('input[type = email]').val())
						;
					} else {
						$form.attr('data-registered', 'false');
						$form.submit();
					}
				}).fail(function(err){
					$form.submit();
				});
			}
		})
	;
});
