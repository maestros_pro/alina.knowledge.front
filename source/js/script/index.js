import $ from 'jquery'
import Popup from '../modules/module.popup'
import Tab from '../modules/module.tab'
import Form from '../modules/module.validate'
import Inputmask from 'inputmask'
import ObjectFit from '../modules/module.objectfit'
import Scrollbar from '../modules/module.scrollbar'
import Viewport from '../modules/module.viewport'
import StickySidebar from 'sticky-sidebar'
import 'slick-carousel'
import 'lightgallery'
import 'lg-video'
import 'lg-thumbnail'


window.app = window.app || {};

if ("ontouchstart" in document.documentElement){
	document.body.className += ' touch';
}


const form = new Form();

[].forEach.call(
	document.querySelectorAll('.form'),
	(el)=>{

		// console.log(el);
		let wrong = false;

		[].forEach.call(
			el.querySelectorAll('input'),
			(input)=>{
				if (input.hasAttribute('data-require')){
					let rule = input.getAttribute('data-require');
					if ( rule ){
						form.validate(input, rule, err =>{ if (err.errors.length) { wrong = true; } });
						input.addEventListener('keyup', check, false);
						input.addEventListener('input', check, false);
						input.addEventListener('change', check, false);
					}

					function check() {
						wrong = false;

						[].forEach.call(
							el.querySelectorAll('input'),
							(input)=>{
								if (input.hasAttribute('data-require')){
									let rule = input.getAttribute('data-require');
									if ( rule ) form.validate(input, rule, err =>{ if (err.errors.length) {wrong = true;} });
								}
							});

						if ( wrong ){
							el.dataset.formValidateError = 'true';
						} else {
							el.removeAttribute('data-form-validate-error');
						}
					}
				}
			});

		if ( wrong ){
			el.dataset.formValidateError = 'true';
		} else {
			el.removeAttribute('data-form-validate-error');
		}

	});



$(function () {

	Inputmask.extendDefinitions({
		'%': {  //masksymbol
			"validator": "[0-9A-Za-zА-яЁёÀ-ÿµ_-]"
		}
	});

	window.app.popup = new Popup({
		bodyClass: 'body-popup',
		onPopupOpen: (pop)=>{
			// console.info(pop);
			$(pop).find('.is-registered').removeClass('is-registered');
			$(pop).find('[data-registered]').removeAttr('data-registered');
		}
	});

	new ObjectFit();
	new Tab({
		classActiveLink: 'is-active',
		classActiveTab: 'is-active',
		onTabChange: (tab)=>{
			// console.info(tab);
			$(tab).removeClass('is-registered');
			$(tab).find('[data-registered]').removeAttr('data-registered');
		}
	});

	const scrollbar = new Scrollbar();

	scrollbar.destroy('.newsfeed__list');
	scrollbar.init('.newsfeed__list');

	new Viewport({
		'0': ()=>{
			scrollbar.destroy('.newsfeed__list');
			scrollbar.init('.newsfeed__list',{
				axis:"x",
				advanced:{
					autoExpandHorizontalScroll:true
				}
			});

		},
		'1010': ()=>{
			scrollbar.destroy('.newsfeed__list');
			scrollbar.init('.newsfeed__list');
		}
	});


	if ( $('.section-article__aside').length ){
		setTimeout(()=>{
			new StickySidebar('.section-article__aside', {
				topSpacing: $('.nav__scroll').length ? 80 : 20,
				bottomSpacing: 20,
				containerSelector: '.section-article .inner',
				innerWrapperSelector: '.section-article__aside-inner'
			});
		}, 10);
	}


	$('.card_carousel').each(function () {
		let $wrap = $(this),
			$slider = $wrap.find('.card__list')
		;

		$slider.slick({
			dots: false,
			infinite: true,
			swipeToSlide: true,
			speed: 300,
			prevArrow: $wrap.find('.card__control-btn_prev'),
			nextArrow: $wrap.find('.card__control-btn_next'),
			variableWidth: false,
			slidesToScroll: 1,
			mobileFirst: true,

			responsive: [
				{
					breakpoint: 780,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 1000,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 1290,
					settings: {
						slidesToShow: 5
					}
				}
			]
		});
	});

	$('.article__img-carousel').each(function () {
		let $wrap = $(this),
			$slider = $wrap.find('.article__img-list'),
			$counter = $wrap.closest('.article__img').find('.time')
		;

		$slider
		.on('init', function (event, slick) {
			if( $counter.length ) $counter.html((slick.currentSlide+1) + '/' + slick.slideCount);
		})
		.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
			if( $counter.length ) $counter.html((nextSlide+1) + '/' + slick.slideCount);
		})
		.slick({
			dots: false,
			infinite: true,
			swipeToSlide: true,
			speed: 300,
			prevArrow: $wrap.find('.card__control-btn_prev'),
			nextArrow: $wrap.find('.card__control-btn_next'),
			variableWidth: false,
			slidesToScroll: 1
		});
	});

	let $b = $('body');

	$('input[type = tel]').each(function () {
		let inputmask = new Inputmask({
			mask: '+7 (999) 999-99-99',
			showMaskOnHover: false,
			onincomplete: function() {
				this.value = '';
				$(this).closest('.form__field').removeClass('f-filled');
			}
		});
		inputmask.mask($(this)[0]);
	});

	$('[data-mask]').each(function () {
		let $t = $(this),
			inputmask = new Inputmask({
				mask: $t.attr('data-mask'),
				placeholder: " ",
				showMaskOnHover: false,
				onincomplete: function() {
					this.value = '';
					$t.closest('.form__field').removeClass('f-filled');
				}
		});
		inputmask.mask($(this)[0]);
	});



	$('.form__field input, .form__field textarea').each(function (e) {
		let $input = $(this),
			$field = $input.closest('.form__field');

		setTimeout(()=>{

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		}, 100);

	});

	$b

		.on('click', function (e) {

			closeOuterClick('.nav__sort-list');
			closeOuterClick('.header__user');

			function closeOuterClick(el) {
				if ( $(el).filter('.is-open').length && !$(e.target).closest(el).length ){
					$(el).filter('.is-open').removeClass('is-open');
				}
			}
		})

		.on('click', '[data-scroll]', function (e) {
			let $t = $(this),
				hash = $t.attr('data-scroll');

			if ( $(hash).length > 0){
				e.preventDefault();
				$('html,body').animate({scrollTop:$(hash).offset().top + 1}, 500);
			}
		})
		.on('click', '.header__handler, .js-nav-handler', function (e) {
			$b.toggleClass('is-menu-open');
		})

		.on('click', '.js-popup-login', function (e) {
			let pop = window.app.popup.open('#authPopup');
			$(pop).find('[data-tab-link=auth_login]').click();
		})
		.on('click', '.js-popup-reg', function (e) {
			let pop = window.app.popup.open('#authPopup');
			$(pop).find('[data-tab-link=auth_reg]').click();
		})

		.on('keyup', '.search input', function (e) {
			let $input = $(this),
				$wrap = $input.closest('.search')
			;

			if ( $.trim($input.val()).length ){
				$wrap.addClass('is-fill')
			} else {
				$wrap.removeClass('is-fill')
			}
		})

		.on('focus', '.search input', function (e) {
			let $input = $(this),
				$wrap = $input.closest('.search')
			;

			if ( !$wrap.hasClass('is-open') ){
				$wrap.addClass('is-open')
			}
		})

		.on('blur', '.search input', function (e) {
			let $input = $(this),
				$wrap = $input.closest('.search')
			;

			if ( $wrap.hasClass('is-open') && !$wrap.hasClass('is-fill') ){
				$wrap.removeClass('is-open')
			}
		})

		.on('click', '.header__user-logo', function (e) {
			let $t = $(this),
				$wrap = $t.closest('.header__user')
			;

			if ($wrap.hasClass('is-open')){
				$wrap.removeClass('is-open');
			} else {
				$wrap.addClass('is-open');
			}

		})


		.on('click', '.search__ico', function (e) {
			let $t = $(this),
				$wrap = $t.closest('.search'),
				$input = $wrap.find('input')
			;

			if ($wrap.hasClass('is-open')){
				$wrap.removeClass('is-open is-fill');
				$input.val('');
			} else {
				$wrap.addClass('is-open');
				$input.focus();
			}

		})

		.on('click', '.nav__sort-item', function (e) {
			let $t = $(this),
				$wrap = $t.closest('.nav__sort-list')
			;

			if ( $wrap.hasClass('is-open') ){
				$t.addClass('is-active').siblings().removeClass('is-active');
				$wrap.removeClass('is-open');
			} else {
				e.preventDefault();
				$wrap.addClass('is-open');
			}

		})




		.on('blur', '.form__field input, .form__field textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		})

		.on('change', '.popup__conditer-item-input', function(e){
			let $t = $(this),
				$popup = $t.closest('.popup'),
				data = $t.closest('.popup__conditer-item').attr('data-item'),
				parse
			;

			$popup.find('.popup__conditer-item').removeClass('is-active');
			$t.closest('.popup__conditer-item').addClass('is-active');

			try{
				parse = JSON.parse(data);
			} catch (e){
				console.error('ошибка парсинга', e);
			}
			if ( data && parse){
				$.each(parse, (key, val)=>{


					if ( key.indexOf('data-') === 0 ){
						let $el = $(`[${key}]`);
						$el.attr(key, val);
					} else {
						let $el = $(`[data-${key}]`);

						if (val && val !== false && val !== 'false'){
							$el.html(val).closest('[data-wrap]').removeClass('is-hidden');
						} else if ( val === false || val === 'false' ){
							$el.html('').closest('[data-wrap]').addClass('is-hidden');
						}
					}

				})
			}
		})
		.on('mouseover', '.hint', function(e){
			let $t = $(this),
				$content = $t.find('.hint__content')
			;

			$t.addClass('is-hover');
			$content.removeAttr('style');

			if( $t.offset().left + $content.outerWidth(true) + 40 >  window.innerWidth ){
				$content.css({marginLeft: window.innerWidth - ($t.offset().left + $content.outerWidth(true) + 40) + 'px' })
			}

		})
		.on('mouseleave', '.hint', function(e){
			let $t = $(this),
				$content = $t.find('.hint__content')
			;
			$t.removeClass('is-hover');

		})
		.on('click', '.base-faq__item-title', function(e){
			let $wrap = $(this).closest('.base-faq__item');

			if ( !$wrap.hasClass('in-action') ){
				$wrap.addClass('in-action');

				$wrap.find('.base-faq__item-text').slideToggle(400, ()=>{
					$wrap.toggleClass('in-action base-faq__item_open').removeAttr('style');
				})
			}

		})
		.on('click', '.btn_disable', function(e){
			e.preventDefault();
		})

		.on('change', '.base-subscribe__item-checkbox input', function(e){
			let $t = $(this), $wrap = $t.closest('.base-subscribe__item');

			if ( $t.prop('checked') ){
				$wrap.find('.btn').removeClass('btn_disable')
			} else {
				$wrap.find('.btn').addClass('btn_disable')
			}

		})

		.on('submit', '.form', function (e) {
			/*
			let $form = $(this),
				$item = $form.find('input'),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');
				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							$(input)
								.closest('.form__field')
								.addClass('f-error f-message')
								.find('.form__message')
								.html(err.errors[0])
							;
						}
					})
				}
			});

			if ( wrong ){
				e.preventDefault();
			} else if( $form.hasClass('form_reg') && !$form.attr('data-registered') ) {
				e.preventDefault();
				$.ajax({
					url: $form.attr('data-check-url'),
					data: $form.serialize()
				}).done(function(data){
					if (data.status === true){
						$form
							.attr('data-registered', 'true')
							.closest('.popup__auth')
							.addClass('is-registered')
							.find('.form_pass')
							.find('input[type = email]')
							.val($form.find('input[type = email]').val())
						;
					} else {
						$form.attr('data-registered', 'false');
						$form.submit();
					}
				}).fail(function(err){
					$form.submit();
				});
			}
			*/
		})
	;


	(()=>{

		let $nav = $('.nav'),
			$window = $(window)
		;
		if ( $nav.length ){
			$window.on('scroll resize', ()=>{
				if ( $window.scrollTop() > $nav.offset().top && !$nav.hasClass('is-fixed')){
					$nav.addClass('is-fixed');
				} else if ( ($window.scrollTop() <= $nav.offset().top) && $nav.hasClass('is-fixed')) {
					$nav.removeClass('is-fixed');
				}
			});
		}


	})();


	$(window).on('load', ()=>{
		setTimeout(()=>{$(window).trigger('resize');}, 5000);
	})




	// $('.popup__conditer-courses').each(function () {
	// 	let $slider = $(this);
	//
	// 	$slider.slick({
	// 		dots: false,
	// 		infinite: false,
	// 		swipeToSlide: true,
	// 		speed: 300,
	// 		arrows: false,
	// 		variableWidth: false,
	// 		slidesToScroll: 1,
	// 		mobileFirst: true,
	//
	// 		responsive: [
	// 			{
	// 				breakpoint: 780,
	// 				settings: {
	// 					slidesToShow: 3
	// 				}
	// 			}
	// 		]
	// 	});
	// });

	$('.base-found__list').each(function () {
		let $wrap = $(this),
			$slider = $wrap.find('.base-found__slider'),
			$counter = $wrap.find('.base-found__control-counter')
		;

		$slider
			.on('init', function (event, slick) {
				if( $counter.length ) $counter.html((slick.currentSlide+1) + '/' + slick.slideCount);
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				if( $counter.length ) $counter.html((nextSlide+1) + '/' + slick.slideCount);
			})
			.slick({
				dots: false,
				infinite: true,
				swipeToSlide: true,
				speed: 300,
				prevArrow: $wrap.find('.base-found__control-arrow_prev').append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M11.3,23.5a1.541,1.541,0,0,0,1.1-.47,1.641,1.641,0,0,0,0-2.27l-8.513-8.78L12.4,3.2a1.641,1.641,0,0,0,0-2.27,1.517,1.517,0,0,0-2.2,0L.59,10.845a1.641,1.641,0,0,0,0,2.27l9.614,9.92a1.54,1.54,0,0,0,1.1.47Z"  transform="translate(22 18)"/></svg>'),
				nextArrow: $wrap.find('.base-found__control-arrow_next').append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M1.7,23.5a1.54,1.54,0,0,1-1.1-.47,1.641,1.641,0,0,1,0-2.27l8.513-8.78L.6,3.2A1.641,1.641,0,0,1,.6.935a1.517,1.517,0,0,1,2.2,0l9.614,9.91a1.641,1.641,0,0,1,0,2.27L2.8,23.035a1.541,1.541,0,0,1-1.1.47Z" transform="translate(25 17)"/></svg>'),
				variableWidth: false,
				slidesToShow: 2,
				slidesToScroll: 1,
				mobileFirst: true,

				responsive: [
					{
						breakpoint: 940,
						settings: {
							slidesToShow: 4
						}
					}
				]
			});
	});

	$('.base-tops__description').each(function () {
		let $wrap = $(this).closest('.base-tops'),
			$slider = $wrap.find('.base-tops__slider'),
			$sliderImages = $wrap.find('.base-tops__image'),
			$counter = $wrap.find('.base-tops__control-counter')
		;

		$slider
			.on('init', function (event, slick) {
				if( $counter.length ) $counter.html((slick.currentSlide+1) + '/' + slick.slideCount);
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				if( $counter.length ) $counter.html((nextSlide+1) + '/' + slick.slideCount);
			})
			.slick({
				dots: false,
				infinite: true,
				swipeToSlide: true,
				speed: 300,
				prevArrow: $wrap.find('.base-tops__control-arrow_prev').append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M11.3,23.5a1.541,1.541,0,0,0,1.1-.47,1.641,1.641,0,0,0,0-2.27l-8.513-8.78L12.4,3.2a1.641,1.641,0,0,0,0-2.27,1.517,1.517,0,0,0-2.2,0L.59,10.845a1.641,1.641,0,0,0,0,2.27l9.614,9.92a1.54,1.54,0,0,0,1.1.47Z"  transform="translate(22 18)"/></svg>'),
				nextArrow: $wrap.find('.base-tops__control-arrow_next').append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M1.7,23.5a1.54,1.54,0,0,1-1.1-.47,1.641,1.641,0,0,1,0-2.27l8.513-8.78L.6,3.2A1.641,1.641,0,0,1,.6.935a1.517,1.517,0,0,1,2.2,0l9.614,9.91a1.641,1.641,0,0,1,0,2.27L2.8,23.035a1.541,1.541,0,0,1-1.1.47Z" transform="translate(25 17)"/></svg>'),
				variableWidth: false,
				slidesToShow: 1,
				adaptiveHeight: true,
				asNavFor: $sliderImages,
				slidesToScroll: 1,
			});

		$sliderImages
			.slick({
				dots: false,
				infinite: true,
				swipeToSlide: false,
				swipe: false,
				speed: 300,
				fade: true,
				arrows: false,
				variableWidth: false,
				slidesToShow: 1,
				adaptiveHeight: true,
				asNavFor: $slider,
				slidesToScroll: 1,
			});
	});

	$('.base-feedbacks__list').each(function () {
		let $wrap = $(this),
			$slider = $wrap.find('.base-feedbacks__slider'),
			$counter = $wrap.find('.base-feedbacks__control-counter')
		;

		$slider
			.on('init', function (event, slick) {
				if( $counter.length ) $counter.html((slick.currentSlide+1) + '/' + slick.slideCount);
				gallery($slider, 'a');
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				if( $counter.length ) $counter.html((nextSlide+1) + '/' + slick.slideCount);
			})
			.slick({
				lazyLoad: 'ondemand',
				dots: false,
				infinite: true,
				swipeToSlide: true,
				speed: 300,
				prevArrow: $wrap.find('.base-feedbacks__control-arrow_prev').append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M11.3,23.5a1.541,1.541,0,0,0,1.1-.47,1.641,1.641,0,0,0,0-2.27l-8.513-8.78L12.4,3.2a1.641,1.641,0,0,0,0-2.27,1.517,1.517,0,0,0-2.2,0L.59,10.845a1.641,1.641,0,0,0,0,2.27l9.614,9.92a1.54,1.54,0,0,0,1.1.47Z"  transform="translate(22 18)"/></svg>'),
				nextArrow: $wrap.find('.base-feedbacks__control-arrow_next').append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><path d="M1.7,23.5a1.54,1.54,0,0,1-1.1-.47,1.641,1.641,0,0,1,0-2.27l8.513-8.78L.6,3.2A1.641,1.641,0,0,1,.6.935a1.517,1.517,0,0,1,2.2,0l9.614,9.91a1.641,1.641,0,0,1,0,2.27L2.8,23.035a1.541,1.541,0,0,1-1.1.47Z" transform="translate(25 17)"/></svg>'),
				// variableWidth: true,
				slidesToShow: 1,
				// adaptiveHeight: false,
				slidesToScroll: 1,
				// mobileFirst: true,

				// responsive: [
				// 	{
				// 		breakpoint: 940,
				// 		settings: {
				// 			variableWidth: true,
				// 		}
				// 	}
				// ]
			});
	});



	function gallery(wrap, item) {

		$(wrap).each((i, e)=>{
			//-console.info(e);
			$(e).eq(i).lightGallery({
				counter: false,
				download: false,
				loop: true,
				hideControlOnEnd: true,
				selector: item,
				youtubePlayerParams: {
					modestbranding: 1,
					showinfo: 0,
					rel: 0,
					controls: 0
				},
				vimeoPlayerParams: {
					byline : 0,
					portrait : 0,
					color : 'A90707'
				},

				thumbnail:true,
				// dynamic:true,
			})
		});

	}

});

