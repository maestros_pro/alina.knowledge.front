import $ from 'jquery'
import Popup from '../modules/module.popup'
import Tab from '../modules/module.tab'
import Form from '../modules/module.validate'

window.registration = window.registration || {};

const form = new Form();

[].forEach.call(
	document.querySelectorAll('.registration-popups .form'),
	(el)=>{

		// console.log(el);
		let wrong = false;

		[].forEach.call(
			el.querySelectorAll('input'),
			(input)=>{
				if (input.hasAttribute('data-require')){
					let rule = input.getAttribute('data-require');
					if ( rule ){
						form.validate(input, rule, err =>{ if (err.errors.length) { wrong = true; } });
						input.addEventListener('keyup', check, false);
						input.addEventListener('input', check, false);
						input.addEventListener('change', check, false);
					}

					function check() {
						wrong = false;

						[].forEach.call(
							el.querySelectorAll('input'),
							(input)=>{
								if (input.hasAttribute('data-require')){
									let rule = input.getAttribute('data-require');
									if ( rule ) form.validate(input, rule, err =>{ if (err.errors.length) {wrong = true;} });
								}
							});

						if ( wrong ){
							el.dataset.formValidateError = 'true';
						} else {
							el.removeAttribute('data-form-validate-error');
						}
					}
				}
			});

		if ( wrong ){
			el.dataset.formValidateError = 'true';
		} else {
			el.removeAttribute('data-form-validate-error');
		}

	});


$(function () {

	window.registration.popup = new Popup({
		bodyClass: 'body-popup',
		onPopupOpen: (pop)=>{
			$(pop).find('.is-registered').removeClass('is-registered');
			$(pop).find('[data-registered]').removeAttr('data-registered');
		}
	});

	new Tab({
		classActiveLink: 'is-active',
		classActiveTab: 'is-active',
		onTabChange: (tab)=>{
			$(tab).removeClass('is-registered');
			$(tab).find('[data-registered]').removeAttr('data-registered');
		}
	});


	let $b = $('body');

	$b

		.on('click', '.js-popup-login', function (e) {
			let pop = window.registration.popup.open('.registration-popups #authPopup');
			$(pop).find('[data-tab-link=auth_login]').click();
		})
		.on('click', '.js-popup-reg', function (e) {
			let pop = window.registration.popup.open('.registration-popups #authPopup');
			$(pop).find('[data-tab-link=auth_reg]').click();
		})

		.on('blur', '.registration-popups .form__field input, .registration-popups .form__field textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		})

		.on('submit', '.registration-popups .form', function (e) {

			let $form = $(this),
				$item = $form.find('input'),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');
				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							$(input)
								.closest('.form__field')
								.addClass('f-error f-message')
								.find('.form__message')
								.html(err.errors[0])
							;
						}
					})
				}
			});
		})
	;



});

